<TabbedPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
            xmlns:vm ="clr-namespace:MisVentas2.ViewModels"
             x:Class="MisVentas2.Vistas.ViewMenu">
  
  
    <ContentPage Title="Ventas" IconImageSource="money.png">
        
    </ContentPage>
    <ContentPage Title="Clientes" IconImageSource="client.png"/>
    <ContentPage Title="Productos" IconImageSource="box.png">
        <ContentPage.BindingContext>
            <vm:ProductosViewModel/>
        </ContentPage.BindingContext>
        <Grid>
            <Grid.RowDefinitions>
                <RowDefinition Height="auto"/>
                <RowDefinition Height="*"/>
            </Grid.RowDefinitions>

            <StackLayout Orientation="Horizontal" Grid.Row="0">
                <Label Text="Buscar:" />
                <Entry x:Name="enBuscar" WidthRequest="100"/>
            </StackLayout>
            <ListView Grid.Row="1" ItemsSource="{Binding Productos}">
                <ListView.ItemTemplate>
                    <DataTemplate>
                        <Grid>
                            <Grid.RowDefinitions>
                                <RowDefinition Height="auto"/>
                                <RowDefinition Height="auto"/>
                            </Grid.RowDefinitions>

                            <StackLayout Orientation="Horizontal" Grid.Row="0">
                                <Label Text="Existencia:"/>
                                <Label Text="{Binding Existencia}"/>
                                <Label Text="{Binding Nombre}" FontAttributes="Bold"/>
                                <Label Text="{Binding costo, StringFormat='{0:$#,##0.00}'}"/>
                            </StackLayout>
                            <Label Text="{Binding Descripcion}" FontAttributes="Bold" Grid.Row="1"/>
                        </Grid>
                    </DataTemplate>
                </ListView.ItemTemplate>

            </ListView>
        </Grid>
    </ContentPage>
    <ContentPage Title="Cuentas por Pagar" IconImageSource=""/>
</TabbedPage>
